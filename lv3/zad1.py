import pandas as pd
import numpy as np

data = pd.read_csv ("data_C02_emission.csv")

# a
print(data.head(10))
print(f"Broj mjerenja: {len(data)}")

# b
data = data.sort_values(by=["Fuel Consumption City (L/100km)"])
print("---Highest consumption:")
print(data.head(3)[["Make", "Model", "Fuel Consumption City (L/100km)"]])
print("---Lowest consumption:")
print(data.tail(3)[["Make", "Model", "Fuel Consumption City (L/100km)"]])

# c
print("---Motor size between 2.5 and 3.5 L:")
new_data = data[(data["Engine Size (L)"] > 2.5) & (data["Engine Size (L)"] < 3.5)]
print(new_data[["Make", "Model", "Engine Size (L)"]])

co2_mean = new_data["CO2 Emissions (g/km)"].mean()
print(f"---Average CO2 emission: {co2_mean}")

# d
grouped_make = data.groupby("Make")
print("---grouped by make:")
print(grouped_make.groups["Audi"].count())