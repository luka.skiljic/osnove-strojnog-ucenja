import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# for i in range(1, 7):
#     # ucitaj sliku
#     img = Image.imread(f"imgs\\test_{str(i)}.jpg")

#     # prikazi originalnu sliku
#     plt.figure()
#     plt.title("Originalna slika")
#     plt.imshow(img)
#     plt.tight_layout()
#     plt.show()

#     # pretvori vrijednosti elemenata slike u raspon 0 do 1
#     img = img.astype(np.float64) / 255

#     # transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
#     w,h,d = img.shape
#     img_array = np.reshape(img, (w*h, d))

#     # rezultatna slika
#     img_array_aprox = img_array.copy()

#     print(f"Broj razlicitih boja: {len(img_array_aprox)}")

#     # k means
#     km = KMeans ( n_clusters =2 , init ="random", n_init =5 , random_state =0 )
#     km.fit(img_array_aprox)
#     labels = km . predict ( img_array_aprox )

#     for i in range(len(labels)):
#         img_array_aprox[i] = km.cluster_centers_[labels[i]]

#     plt.figure()
#     plt.title("Nova slika")
#     plt.imshow(img_array_aprox.reshape(w,h,d))
#     plt.tight_layout()
#     plt.show()

#     # J vs K


# ucitaj sliku
img = Image.imread(f"imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

print(f"Broj razlicitih boja: {len(img_array_aprox)}")

j_list = []
k_list = list(range(1, 6))

for k in range(1, 6):
    img_array_aprox = img_array.copy()

    # k means
    km = KMeans ( n_clusters =k , init ="random", n_init =5 , random_state =0 )
    km.fit(img_array_aprox)
    labels = km . predict ( img_array_aprox )
    j_list.append(km.inertia_)

    for i in range(len(labels)):
        img_array_aprox[i] = km.cluster_centers_[labels[i]]

    # plt.figure()
    # plt.title("Nova slika")
    # plt.imshow(img_array_aprox.reshape(w,h,d))
    # plt.tight_layout()
    # plt.show()


# J vs K
plt.figure()
plt.scatter(k_list,j_list)
plt.xlabel('K')
plt.ylabel('J')
plt.show()

