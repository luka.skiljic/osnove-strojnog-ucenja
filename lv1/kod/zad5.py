ham_count = 0
spam_count = 0
spam_shouting_count = 0
ham_words = []
spam_words = []

fhand = open ("../SMSSpamCollection.txt")
for line in fhand :
    line = line . rstrip ()
    #print ( line )
    words = line . split ()

    if(words[0] == "spam"):
        spam_count += 1
        spam_words += words[1:]
        if words[-1][-1] == "!":
            spam_shouting_count += 1
    else:
        ham_count += 1
        ham_words += words[1:]

fhand . close ()

print(f'spam_count: {spam_count}')
print(f'spam_shouting_count: {spam_shouting_count}')
print(f'ham_count: {ham_count}')
print(f'ham_avg_word_count: {len(ham_words) / ham_count}')
print(f'spam_avg_word_count: {len(spam_words) / spam_count}')
