
class InvalidGradeException(Exception):
    pass



# main:

print("Unesite ocjenu:")

try:
    ocjena = float(input())
    
    if ocjena < 0.0 or ocjena > 1.0:
        raise InvalidGradeException
    
    if ocjena >= 0.9:
        print("A")
    elif ocjena >= 0.8:
        print("B")
    elif ocjena >= 0.7:
        print("C")
    elif ocjena >= 0.6:
        print("D")
    else:
        print("F")

except InvalidGradeException:
    print("ERROR: Unesena vrijednost mora biti između 0 i 1.")

except ValueError:
    print("ERROR: Invalid value.")
