
numbers = []

print("Unesi nesta:")
_ = input()
while(_ != "Done"):
    try:
        numbers.append(float(_))
    except ValueError:
        print("Unesena vrijednost treba biti broj!!!")

    print("Unesi nesta:")
    _ = input()

print(f'Broj brojeva: {len(numbers)}')
print(f'Srednja vrijednost: {sum(numbers) / len(numbers)}')
print(f'Min: {min(numbers)}')
print(f'Max: {max(numbers)}')

numbers.sort()
print(f'numbers: \n{numbers}')
