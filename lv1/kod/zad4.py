
word_frequencies = dict()

fhand = open ("../song.txt")
for line in fhand :
    line = line . rstrip ()
    print ( line )
    words = line . split ()

    for w in words:
        if w in word_frequencies.keys():
            word_frequencies[w] += 1
        else:
            word_frequencies[w] = 1

fhand . close ()

print("Single use words:\n")
for key in word_frequencies.keys():
    if word_frequencies[key] == 1:
        print(key)