from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler
from sklearn . preprocessing import OneHotEncoder
import pandas as pd

# ucitavanje dataseta
data = pd.read_csv("data_C02_emission.csv")
print(data.columns)

X = data[['Engine Size (L)', 'Cylinders', 'Fuel Consumption City (L/100km)',
       'Fuel Consumption Hwy (L/100km)', 'Fuel Consumption Comb (L/100km)',
       'Fuel Consumption Comb (mpg)']]
y = data['CO2 Emissions (g/km)']

X_train, X_test, y_train, y_test = train_test_split (X , y , test_size = 0.2 , random_state =1 )

print(X_train.head(10))
print(X_test.head(10))
