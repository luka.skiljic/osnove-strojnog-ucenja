#import bibilioteka
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay



##################################################
#1. zadatak
##################################################
data = pd.read_csv("winequality-red.csv", sep=";")

#a)
wine_count = len(data)
print(f"Mjerenje je provedeno na {wine_count} vina.")

#b)
# plt.figure()
# data.alcohol.plot(kind="hist", bins=30)
# plt.xlabel("Alcohol")
# plt.ylabel("wine instances")
# plt.title("Alcohol distribution")
# plt.show()

#c)
a = len(data[data.quality < 6])
b = len(data[data.quality >= 6])
print(f"Broj uzoraka s kvalitetom manjom od 6: {a}")
print(f"Broj uzoraka s kvalitetom vecom ili jednakom od 6: {b}")

#d)
#print(data.corr(numeric_only=True))

##################################################
#2. zadatak
##################################################


#a)

#b)

#c)


##################################################
#3. zadatak
##################################################

# sazimanje izlaza na 2 klase
data.quality = np.where(data.quality < 6, 0, data.quality)
data.quality = np.where(data.quality >= 6, 1, data.quality)

x = data.copy().drop("quality", axis="columns")
y = data.quality.copy()

x_train, x_test, y_train, y_test = train_test_split(x , y , test_size = 0.2 , random_state=1)
#data.info()

#a)
model = keras.Sequential()
model.add(layers.Input(shape=(11,)))
model.add(layers.Dense(22, activation="relu"))
model.add(layers.Dense(12, activation="relu"))
model.add(layers.Dense(4, activation="relu"))
model.add(layers.Dense(1, activation="sigmoid"))
#model.summary()

#b)
model.compile(optimizer='adam',
                loss='binary_crossentropy',
                metrics=['accuracy'])

#c)
# model.fit(x_train,
#             y_train,
#             epochs = 800,
#             batch_size = 50,
#             validation_split = 0.1)

#d)
# model.save("./model/")
del model

#e)
model = keras.models.load_model("./model/")

score = model.evaluate(x_test, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')

#f)
predictions = model.predict(x_test)

y_pred = np.argmax(predictions, axis=1)
cm = confusion_matrix(y_test, y_pred)
disp = ConfusionMatrixDisplay(confusion_matrix=cm)
disp.plot()
plt.show()