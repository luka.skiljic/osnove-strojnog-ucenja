import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")

img = img[:,:,0].copy()

print(img)

# plt.figure()
# plt.imshow(img, cmap="gray")
# plt.show()


# a) posvijetliti sliku
for i in range(len(img)):
    for j in range(len(img[0])):
        img[i, j] = img[i, j] + 50
        img[i, j] = min(255, img[i, j])

# plt.figure()
# plt.imshow(img, cmap="gray")
# plt.show()

# prikazati drugu cetvrtinu

width = len(img[0])
height = len(img)
width_fourth = int(width / 4)

img_quarter = img[:,width_fourth:width_fourth * 2].copy()
print(img_quarter)

# plt.figure()
# plt.imshow(img_quarter, cmap="gray")
# plt.show()

# zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu

img_rotated = [[0] * height] * width
img_rotated = np.array(img_rotated)
for row in range(height):
    for column in range(width):
        img_rotated[column, height - row - 1] = img[row, column]

# plt.figure()
# plt.imshow(img_rotated, cmap="gray")
# plt.show()

# zrcaliti sliku

img_mirrored = img.copy()
for row in range(height):
    for column in range(width):
        img_mirrored[row, width - column - 1] = img[row, column]

# plt.figure()
# plt.imshow(img_mirrored, cmap="gray")
# plt.show()

# crni i bijeli kvadrati


black_square = np.zeros((50,50), dtype=int)
white_square = np.ones((50,50), dtype=int)
img_squares = np.vstack(
    (
        np.hstack((black_square, white_square)),
        np.hstack((white_square, black_square))
    )
)

plt.figure()
plt.imshow(img_squares, cmap="gray")
plt.show()