import numpy as np
import matplotlib.pyplot as plt
import csv

with open("data.csv", "r") as f:
    reader = csv.reader(f)
    data = np.array(list(reader))

print(data[0:5, 1])

print(data[0])
print(f"Broj ispitanika: {len(data) - 1}")

plt.scatter(data[1::50, 1].astype(float), data[1::50, 2].astype(float))
plt.xlabel("height")
plt.ylabel("weight")
plt.show()

heights = np.array(data[1:, 1], float)

print(f"Height min: {heights.min()}")
print(f"Height max: {heights.max()}")
print(f"Height mean: {heights.mean()}")

ind = data[1:, 0].astype(float) == 1

male_heights = []
female_height =  []
for i in range(len(heights)):
    if(ind[i]):
        male_heights.append(heights[i])
    else:
        female_height.append(heights[i])

male_h = np.array(male_heights, float)
female_h = np.array(female_height, float)

print()
print(f"Male height min: {male_h.min()}")
print(f"Male height max: {male_h.max()}")
print(f"Male height mean: {male_h.mean()}")

print()
print(f"Female height min: {female_h.min()}")
print(f"Female height max: {female_h.max()}")
print(f"Female height mean: {female_h.mean()}")
